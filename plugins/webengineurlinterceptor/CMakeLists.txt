# SPDX-FileCopyrightText: 2015-2021 Laurent Montel <montel@kde.org>
# SPDX-License-Identifier: BSD-3-Clause
add_definitions(-DTRANSLATION_DOMAIN=\"webengineurlinterceptor\")
add_subdirectory(adblock)
add_subdirectory(donottrack)
